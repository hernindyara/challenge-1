const dataPenjualanPakAldi = [
{
    namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
    hargaSatuan : 760000,
    kategori : "Sepatu Sport",
    totalTerjual : 90,
},
{
    namaProduct : 'Sepatu Warrior Tristan Black Brown High',
    hargaSatuan : 960000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 37,
},
{
    namaProduct : 'Sepatu Warrior Tristan Maroon High',
    hargaSatuan : 360000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 90,
},
{
    namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
    hargaSatuan : 120000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 90,
}
]
const getTotalPenjualan = dataPenjualan => {
    if (typeof dataPenjualan != 'object') {
        return string = 'Invalid type data'
    }
    else {
        let totalPrice = dataPenjualan.reduce(function (accumulator, item) {
            return accumulator + item.totalTerjual;
          }, 0);
    
        return totalPrice
    }
}

console.log(getTotalPenjualan(dataPenjualanPakAldi))
//console.log(getTotalPenjualan())
