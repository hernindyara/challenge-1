const getSplitName = (personName) => {
    if(typeof personName === "string"){
        personName = personName.split(" ")
        if(personName.length == 3){
            return {firstName: personName[0] ,middleName: personName[1] ,lastName: personName[2]}
        } else if (personName.length == 2){
            return {firstName: personName[0] ,middleName: personName[1] ,lastName: null}
        } else if (personName.length == 1){
            return {firstName: personName[0] ,middleName: null ,lastName: null}
        } else {
           return Error("This Function Just Only for 3 character") 
        }
    } else {
        return Error("Invalid Data Type")
    }
}

console.log(getSplitName("Aldi Daniela Pranata"))
console.log(getSplitName("Aldi Kuncoro"))
console.log(getSplitName("Aurora"))
console.log(getSplitName("Aurora Aureliya Sukma Darma"))
console.log(getSplitName(0))
