function getAngkaTerbesarKedua(givenNumber){
    if(typeof givenNumber == "object"){
        givenNumber.sort(function(a, b){return b-a})
        return givenNumber[1]
    } else {
        return Error("Invalid data type")
    }
}
const dataAngka = [9,4,7,7,4,3,2,2,8]
console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())